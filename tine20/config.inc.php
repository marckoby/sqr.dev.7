<?php
return array (
  'captcha' => 
  array (
    'count' => 0,
  ),
  'database' => 
  array (
    'host' => 'localhost',
    'dbname' => 'tine20',
    'username' => 'tine20',
    'password' => 'aJfujunzIoqA3P5zMb2',
    'adapter' => 'pdo_mysql',
    'tableprefix' => 'tine20_',
    'port' => '',
  ),
  'setupuser' => 
  array (
    'username' => 'tine20setup',
    'password' => 'a0f848942ce863cf53c0fa6cc684007d',
  ),
  'caching' => 
  array (
    'active' => true,
    'path' => '/var/lib/tine20/cache',
    'lifetime' => 3600,
    'backend' => 'File',
    'redis' => 
    array (
      'host' => 'localhost',
      'port' => 6379,
    ),
    'memcached' => 
    array (
      'host' => 'localhost',
      'port' => 11211,
    ),
  ),
  'logger' => 
  array (
    'active' => true,
    'filename' => '/var/log/tine20/tine20.log',
    'priority' => '3',
  ),
  'tmpdir' => '/var/lib/tine20/tmp',
  'sessiondir' => '/var/lib/tine20/sessions',
  'filesdir' => '/var/lib/tine20/files',
  'mapPanel' => 1,
  'actionqueue' => 
  array (
    'active' => false,
    'backend' => 'Redis',
    'host' => 'localhost',
    'port' => 6379,
  ),
  'session' => 
  array (
    'lifetime' => 86400,
    'backend' => 'File',
    'path' => '/var/lib/tine20/sessions',
    'host' => 'localhost',
    'port' => 6379,
  ),
);
