# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command

SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/X11R6/bin:/root/bin
#export 
DEBIAN_FRONTEND=noninteractive


#https://help.ubuntu.com/community/Upside-Down-TernetHowTo
#*/30 * * * * proxy rm -vf /var/www/images/* &> /tmp/flip.log

#*/17 * * * * find /var/www/images -daystart -maxdepth 1 -mmin +159 -type f -iname "*.jpg" -ls -exec rm -vf {} \;   &> /tmp/flip.log
#*/17 * * * * find /var/www/images -daystart -maxdepth 1 -mmin +159 -type f -iname "*.jpg" -ls -delete  &> /tmp/flip.log
*/17 * * * * find /var/www/tmp/ -daystart -maxdepth 1 -mmin +159 -type f -iname "*" -ls -delete  &> /tmp/hotspot.log



# lossless compress cached jpgs
30 23  * * *  (set +x; umask 000;  jpegoptim -pvt $(set -f && echo /home/*/.cache/google-chrome/Cache/*  /home/*/.cache/chromium/Cache/* /home/*/.mozilla/firefox/*/Cache/* /var/spool/squid/??/*/*) | grep -v "[ERROR]") &> /tmp/jpegoptim.log
#33 23 * * *  for t in $(set -f && echo /home/*/.cache/google-chrome/Cache /home/*/.cache/chromium/Cache /home/*/.mozilla/firefox/*/Cache); do [ -d $t ] && jpegoptim -pvt  $t/* | grep -v "[ERROR]"; done &> /tmp/jpegoptim.log

# http://www.mythtv.org/wiki/ACPI_Wakeup
@daily (sudo bash -c "echo $(date '+\%s' -d 'tomorrow 10am') > /sys/class/rtc/rtc0/wakealarm"; cat /proc/driver/rtc)  &> /tmp/wakeup.log

# update cleanup preview
##0 10 * * * (time bleachbit_cli -l | xargs bleachbit_cli -p ) &> /tmp/bleachbit_cli.log <&-

# run for safety, since cron.hourly doesn't seem to always kick in
#30 */6 * * *   /etc/cron.hourly/hamachi.check &> /tmp/hamachi.check.log




# periodic system updates
# time apt-get update; time apt-get  -y --force-yes upgrade &
## service debtorrent-client start; time apt-get -y update; time apt-get -y upgrade ; service debtorrent-client stop &
#0 11 * * * (service debtorrent-client start; time apt-get -y update; time apt-get -f install;aptitude -y full-upgrade;  time apt-get -y --force-yes dist-upgrade; service debtorrent-client stop) &> /tmp/apt-get.log <&-

# THIS is OKAY, but they want to freeze PHP version 5.4.28
#5 10 * * 1-6  DEBIAN_FRONTEND=noninteractive DATETIMESTR="$(date +\%Y-\%m-\%d_\%H-\%M-\%S)" && cd /tmp && touch apt.updates.$DATETIMESTR.log && rm -f apt.updates.log && cp -l apt.updates.$DATETIMESTR.log apt.updates.log && (set -x; time apt-get update; time apt-get -y  --force-yes install -o Dpkg::Options::=--force-confdef; time  apt-get -y  --force-yes -o Dpkg::Options::=--force-confdef upgrade;  time apt-get -y --force-yes dist-upgrade) &> /tmp/apt.updates.log <&-; grep -qs "you must manually run" /tmp/apt.updates.log &&  dpkg --configure -a 2>&1 >> /tmp/apt.updates.log <&-


# manually:

#  DEBIAN_FRONTEND=noninteractive DATETIMESTR="$(date +\%Y-\%m-\%d_\%H-\%M-\%S)" && cd /tmp && touch apt.updates.$DATETIMESTR.log && rm -f apt.updates.log && cp -l apt.updates.$DATETIMESTR.log apt.updates.log && (set -x; time apt-get update ; time  apt-get -y  --force-yes -o Dpkg::Options::=--force-confdef install; time  apt-get -y  --force-yes -o Dpkg::Options::=--force-confdef upgrade; time apt-get -y  --force-yes dist-upgrade) <&- |tee -a /tmp/apt.updates.log


# dpkg -l 'linux-*' | sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' | xargs -t apt-get -y purge  
# http://markmcb.com/2013/02/04/cleanup-unused-linux-kernels-in-ubuntu/

7 11 */13 * *  (dpkg -l 'linux-*' | sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' | xargs -t apt-get -y purge) &> /tmp/clean.old.kernels.log




#http://unix.stackexchange.com/questions/22820/how-to-make-apt-get-accept-new-config-files-in-an-unattended-install-of-debian-f
# -o Dpkg::Options::=--force-confdef


# Save the router config often (later, goes into git)
2 2 * * * timeout 1m expect  -c 'spawn telnet  192.168.1.1 ;  expect "Username:" {send  "admin\n"};  expect "Password:" {send "#Square123\n"};  expect "HomeGateway>" {send "conf print /\n"};  expect "HomeGateway>"'  > /etc/darkstat/HomeGateway/HomeGateway.conf





# squid heartbeat checker
*/4 * * * * (netstat -pan | grep -wq "squid3"  ||  (killall squid3; rm -vf $(ls -1aS /var/spool/squid*/??/*/* | head -n 1); sleep 9; /usr/sbin/service squid3 start))  &> /tmp/squid.check.log
#"3128" false positive: 
#unix  3      [ ]         STREAM     CONNECTED     35747    3128/syndaemon   

