# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command


SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/X11R6/bin:/root/bin:$PATH
#DEBIAN_FRONTEND=noninteractive


#0 * * * * /srv/bitbucket-synch.sh diet
#0 * * * * /srv/bitbucket-synch.sh bike
#0 * * * * /srv/bitbucket-synch.sh audio
#0 * * * * /srv/bitbucket-synch.sh sport
#0 * * * * /srv/bitbucket-synch.sh compare
#0 * * * * /srv/bitbucket-synch.sh trash
#0 * * * * /srv/bitbucket-synch.sh crm
#0 * * * * /srv/bitbucket-synch.sh stack
#0 * * * * /srv/bitbucket-synch.sh core-test

0 7-23 * * *  sleep 10 && /srv/bitbucket-synch.sh core-test	 /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)"	>> /tmp/bitbucket-synch.core-test.log  2>&1
0 7-23 * * *  sleep 20 && /srv/bitbucket-synch.sh diet      /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.diet.log  2>&1
0 7-23 * * *  sleep 30 && /srv/bitbucket-synch.sh bike      /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.bike.log  2>&1
0 7-23 * * *  sleep 40 && /srv/bitbucket-synch.sh audio      /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.audio.log  2>&1
0 7-23 * * *  sleep 50 && /srv/bitbucket-synch.sh sport      /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.sport.log  2>&1
0 7-23 * * *  sleep 60 && /srv/bitbucket-synch.sh compare     /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.compare.log  2>&1
0 7-23 * * *  sleep 70 && /srv/bitbucket-synch.sh trash     /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)"  >> /tmp/bitbucket-synch.trash.log  2>&1
0 7-23 * * *  sleep 80 && /srv/bitbucket-synch.sh crm      /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)" >> /tmp/bitbucket-synch.crm.log  2>&1
0 7-23 * * *  sleep 90 && /srv/bitbucket-synch.sh stack     /"$(date +\%Y-\%m-\%d_\%H-\%M-\%S;tty)"  >> /tmp/bitbucket-synch.stack.log  2>&1


# Record what SQL and code changed when. Neatly!
# clean up logs, basically:
# (cd /srv/logs ; cp -avf /tmp/bitbucket-synch.*.log . ; bzip2 -9vkf bit*.log ; linescollapse bit*.log )
58  * * * *  /srv/synclog.sh &> /tmp/synclog.sh.log



# fire up a git web gui, but only if port 4000 is free
*/40 * * * * (date; lsof -iTCP:4000 || time gitalist_server -r -p 4000 --repo_dir /srv/ ) &> /tmp/gitalist_server.log
